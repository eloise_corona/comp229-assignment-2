import java.awt.*;
import java.util.*;
import java.time.*;
import java.util.List;

import bos.RelativeMove;

class Stage extends KeyObservable {
    protected Grid grid;
    protected Character sheep;
    protected Character shepherd;
    protected Character wolf;
    private List<Character> allCharacters;
    protected Player player;

    protected Memento memento; //creates a protected Memento object
    Caretaker caretaker; //creates a caretaker object for the save/loading of the stage

    private static Stage uniqueInstance = new Stage(); //creates a stage object to check if the stage is unique

    private Instant timeofLastMove = Instant.now(); //records the last move


    /*checks that the stage is unique*/
    public static Stage getInstance(){
        if(uniqueInstance == null){
            uniqueInstance = new Stage(); //if the stage is unique, the stage becomes the unique instance
        }
        return uniqueInstance;
    }



    private Instant timeOfLastMove = Instant.now();

    private Stage() {
        SAWReader sr = new SAWReader("data/stage1.saw");
        grid     = new Grid(10, 10);
        shepherd = new Shepherd(grid.cellAtRowCol(sr.getShepherdLoc().first, sr.getShepherdLoc().second), new StandStill());
        sheep    = new Sheep(grid.cellAtRowCol(sr.getSheepLoc().first, sr.getSheepLoc().second), new MoveTowards(shepherd));
        wolf     = new Wolf(grid.cellAtRowCol(sr.getWolfLoc().first, sr.getWolfLoc().second), new MoveTowards(sheep));

        player = new Player(grid.getRandomCell());
        this.register(player);


       caretaker = new Caretaker(); //creates a new caretaker for the saved stage
       this.register(caretaker);


        allCharacters = new ArrayList<Character>();
        allCharacters.add(sheep); allCharacters.add(shepherd); allCharacters.add(wolf);

    }

    public void update(){
        if (!player.inMove()) {
            if (sheep.location == shepherd.location) {
                System.out.println("The sheep is safe :)");
                System.exit(0);
            } else if (sheep.location == wolf.location) {
                System.out.println("The sheep is dead :(");
                System.exit(1);
            } else {
                if (sheep.location.x == sheep.location.y) {
                    sheep.setBehaviour(new StandStill());
                    shepherd.setBehaviour(new MoveTowards(sheep));
                }
                allCharacters.forEach((c) -> c.aiMove(this).perform());
                player.startMove();
                timeOfLastMove = Instant.now();
            }
        }
    }

    public void paint(Graphics g, Point mouseLocation) {
        grid.paint(g, mouseLocation);
        sheep.paint(g);
        shepherd.paint(g);
        wolf.paint(g);
        player.paint(g);
    }

    /*saves the current stage*/
    public Memento saveStage(){
        memento = new Memento(); //creates a memento to store the current characters
        memento.setStage(player.location, sheep.location, shepherd.location, wolf.location); //sets the stage

        System.out.println(memento.getPlayer());
        System.out.println(memento.getSheep());
        System.out.println(memento.getShepherd());
        System.out.println(memento.getWolf());
        return memento;
    }

    /*loads the previously saved stage*/
    public void loadStage(Memento memento){
        player.location = memento.getPlayer(); //load player
        sheep.location = memento.getSheep(); //load sheep
        shepherd.location = memento.getShepherd(); //load shepherd
        wolf.location = memento.getWolf(); //load wolf
    }
}