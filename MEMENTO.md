MEMENTO PATTERN

Memento.java:
This class is essentially a superclass for the memento pattern which is where the 
instances of the characters in the game are stored. They are protected because
they are only used and needed by the subclasses. The saveStage() method will set
the memento and the loadStage() method will call the location of the characters
and players and set them on the level displayed.

Caretaker.java:
This class does not modify or technically touch the memento. Its role is to save
the memento and load it. It looks after the memento.

Stage.java:
In this memento pattern, stage is the originator. In this class, it creates a
memento and it will set it, which is passed to the Memento class. It has
separate getter methods to call the player, sheep, shepherd and wolf locations
for the memento, and it also has a setter function to set the memento being
created.

-------------------------------------------------------------------------------
WHY IS MEMENTO PATTERN NOT A GOOD CHOICE?

Memento pattern is not a reliable choice because, if you decide to change anything 
within the stage class, it would require the programmer to also edit the memento 
class and caretaker to fit these new changes, making it inefficient in the long run.
It is not flexible to use.