import bos.GameBoard;

public class Caretaker implements KeyObserver{ //uses the keyobserver interface for the notify method
    Stage stage;
    Memento memento;

    /*to initiate save/load*/
    public void notify(char c, GameBoard<Cell> level){
        if(c == ' '){ //if space is pressed, get data of the stage and save
            stage = Stage.getInstance();
            memento = stage.saveStage();
        }

        if(c == 'r' || c == 'R'){ //if r is pressed (or R if caps lock is on), load the saved stage
            stage.loadStage(memento);
        }
    }
}
