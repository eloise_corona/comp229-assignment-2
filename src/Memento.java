public class Memento extends Caretaker {
    protected Cell player;
    protected Cell sheep;
    protected Cell shepherd;
    protected Cell wolf;

/*memento constructors*/
    public void setStage(Cell player, Cell sheep, Cell shepherd, Cell wolf) {
        this.sheep = sheep;
        this.shepherd = shepherd;
        this.wolf = wolf;
        this.player = player;
    }

    public Cell getPlayer(){ //getter for player
        return player;
    }

    public Cell getSheep(){ //getter for sheep
        return sheep;
    }

    public Cell getShepherd(){ //getter for shepherd
        return shepherd;
    }

    public Cell getWolf(){ //getter for wolf
        return wolf;
    }
}